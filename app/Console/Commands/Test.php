<?php

namespace App\Console\Commands;

use App\Notifications\ExpoPushNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;
use YieldStudio\LaravelExpoNotifier\ExpoNotificationsChannel;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expo:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = 'ExponentPushToken[9LEs4HK3yTyji_q5RgKVmX]';
        $noti = new ExpoPushNotification($token);

        Notification::route( ExpoNotificationsChannel::class, $token)->notify($noti);
        return Command::SUCCESS;
    }
}
