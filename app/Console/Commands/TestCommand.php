<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 's3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = storage_path('app/2023/05/05/4782c12abc19287adce0bae12893c9d26238b886.jpg');
        $file = new File($path);
        $dd = Storage::disk('s3')->put('',$file);
        dd($dd);
        return Command::SUCCESS;
    }
}
