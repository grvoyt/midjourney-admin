<div
>
    <div class="border-dashed text-end p-3 picture-actions">

        @if(!empty($value))
        <div class="fields-picture-container">
            <img src="{{ $value }}" class="picture-preview img-fluid img-full mb-2 border" alt="">
        </div>
        @endif

        <span class="mt-1 float-start">{{ __('Upload image from your computer:') }}</span>

        <div class="btn-group">
            <label class="btn btn-default m-0">
                <x-orchid-icon path="cloud-upload" class="me-2"/>

                {{ __('Browse') }}
                <input type="file"
                       name="{{ $name }}"
                       accept="image/*"
                       data-picture-target="upload"
                       data-action="change->picture#upload"
                       class="d-none">
            </label>
        </div>

    </div>

    <input class="picture-path d-none"
           type="text"
           data-picture-target="source"

    >
</div>
