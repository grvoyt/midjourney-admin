<?php

namespace App\Orchid\Layouts\Style;

use App\Clients\StyleDTO;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class StyleListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'styles';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('uuid')
                ->cantHide()
                ->render(fn(StyleDTO $styleDTO) => Link::make($styleDTO->uuid)
                    ->route('platform.styles.edit',$styleDTO->uuid)),
            TD::make('title')
                ->cantHide()
                ->render(fn(StyleDTO $styleDTO) => $styleDTO->title),
            TD::make('available')
                ->cantHide()
                ->render(fn(StyleDTO $styleDTO) => $styleDTO->available),

            TD::make('sort')
                ->cantHide()
                ->render(fn(StyleDTO $styleDTO) => $styleDTO->sort),
        ];
    }
}
