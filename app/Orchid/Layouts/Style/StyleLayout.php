<?php

namespace App\Orchid\Layouts\Style;

use App\Orchid\Components\ImageUploaderComponent;
use App\Orchid\Fields\UploadCustom;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\RadioButtons;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class StyleLayout extends Rows
{
    public $target = 'style';


    protected function fields(): iterable
    {
        $style = $this->query->get('style');

        return [
            Input::make('title')
                ->type('text')
                ->max(250)
                ->required()
                ->title('title')
                ->value($style?->title),

            Input::make('prompt')
                ->type('text')
                ->max(255)
                ->required()
                ->title('prompt')
                ->value($style?->prompt)
                ->placeholder(__('Name')),

            Select::make('available')
                ->title('available')
                ->required()
                ->options([
                    'free' => 'free',
                    'premium' => 'premium',
                    'gold' => 'gold',
                ])
                ->value($style?->available),

            RadioButtons::make('is_new')
                ->title('Новый ли?')
                ->options([
                    0 => __('нет'),
                    1 => __('да'),
                ])
                ->value($style?->isNew),

            Input::make('sort')
                ->title('Сортировка')
                ->type('number')
                ->value($style?->sort),

            ImageUploaderComponent::make('image')
                ->value($style?->image),
        ];
    }
}
