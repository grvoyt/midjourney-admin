<?php

namespace App\Orchid\Components;

use Orchid\Screen\Concerns\Makeable;
use Orchid\Screen\Contracts\Fieldable;
use Orchid\Support\Init;

class ImageUploaderComponent implements Fieldable
{
    use Makeable;

    private array $attributes = [
        'name'     => '',
        'title'    => '',
        'prefix'   => '',
        'value'    => '',
        'id'       => 'project_id',
        'options'  => [],
        'complexes' => [],
    ];

    public function name(?string $name = null)
    {
        $this->attributes['id'] = $name ?? '';
        $this->attributes['name'] = $name ?? '';
        if ($this->attributes['name']) {
            $exp = explode('.', $this->attributes['name']);
            if (count($exp) == 2) {
                $this->attributes['name'] = $exp[0] . '[' . $exp[1] . ']';
            }
        }
        return $this;
    }

    public function render()
    {
        return view('image_render', $this->attributes);
    }

    public function get(string $key, $value = null)
    {
        return '';
    }

    public function set(string $key, $value)
    {
        return $this;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function value(?string $value)
    {
        $this->attributes['value'] = $value;
        return $this;
    }
}
