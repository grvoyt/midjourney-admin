<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Clients\Client;
use App\Orchid\Layouts\Examples\ChartBarExample;
use App\Orchid\Layouts\Examples\ChartLineExample;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class PlatformScreen extends Screen
{
    protected array $metrics = [];
    protected array $queue = [];
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        $api = new Client();
        $this->metrics = $api->getMetrics()->toArray() ?? [];
        $charts = $api->getCharts(['queue','trs'])->toArray() ?? [];
        return [
            'metrics' => $this->metrics,
            'queue'  => [$charts['queue']],
            'trs'  => [$charts['trs']],
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Главная';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Основная статистика';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('AppStore')
                ->href('https://apps.apple.com/ru/app/loor-ai-photo-editor/id6446591629')
                ->icon('globe-alt'),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::metrics([
                'Всего пользователей'    => 'metrics.users',
                'Кол-во транзакций' => 'metrics.transactions',
                'Кол-во картинок отправлено' => 'metrics.queue',
            ]),

            ChartLineExample::make('queue', 'Запросы картинок'),

            ChartLineExample::make('trs', 'Кол-во транзакций'),
        ];
    }
}
