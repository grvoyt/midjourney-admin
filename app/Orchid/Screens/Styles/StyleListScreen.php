<?php

namespace App\Orchid\Screens\Styles;

use App\Clients\Client;
use App\Orchid\Layouts\Style\StyleListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class StyleListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        $client = new Client();
        return [
            'styles' => $client->list()
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Стили';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Добавить')
            ->icon('plus')
            ->route('platform.styles.create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            StyleListLayout::class,
        ];
    }
}
