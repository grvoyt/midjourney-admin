<?php

namespace App\Orchid\Screens\Styles;

use App\Clients\Client;
use App\Clients\CreateRequestDTO;
use App\Orchid\Layouts\Style\StyleLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class StyleEditScreen extends Screen
{
    public ?string $uuid = null;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(?string $uuid = null): iterable
    {
        $client = new Client();
        $style = is_null($uuid)
            ? null
            : $client->show($uuid);

        return compact('style','uuid');
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return is_null($this->uuid)
            ? 'Стиль создание'
            : "Стиль: {$this->uuid}";
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        $commands = [];

        if( !is_null($this->uuid) ) {
            $commands[] = Button::make("Удалить")
                ->type(Color::DANGER())
                ->icon('check')
                ->confirm('Точно удалить?')
                ->method('delete');
        }

        return $commands;
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(StyleLayout::class)
                ->vertical()
                ->commands(
                    Button::make("Сохранить")
                        ->type(Color::DEFAULT())
                        ->icon('check')
                        ->method('save')
                )
        ];
    }

    public function save(Request $request,?string $uuid,Client $client)
    {
        $dto = new CreateRequestDTO();
        $dto->prompt = $request->prompt;
        $dto->title = $request->title;
        $dto->available = $request->available;
        $dto->is_new = (bool) $request->is_new;
        $dto->sort = $request->sort ?? 0;

        if( $request->has('image') ) {
            $image = $request->file('image');
            $path = $image->store('styles','public_for');
            $dto->image = Storage::disk('public_for')->url($path);
        }

        Toast::success('Сохранено');

        if( is_null($uuid) ) {
            $style = $client->create($dto);
            return redirect()->route('platform.styles.edit',$style->uuid);
        } else {
            $client->edit($uuid,$dto);
            return back();
        }
    }

    public function delete(?string $uuid,Client $client)
    {
        $client->delete($uuid);

        Toast::success('Успешно удалено');

        return redirect()->route('platform.styles');
    }
}
