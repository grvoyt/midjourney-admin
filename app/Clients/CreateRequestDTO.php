<?php

namespace App\Clients;

use Illuminate\Support\Arr;

class CreateRequestDTO
{
    public string $title;
    public bool $is_new;
    public ?string $image = null;
    public string $available;
    public string $prompt;
    public int $sort = 0;

    public static function from(array $data)
    {
        $obj = new self;
        $fields = [
            'title',
            'is_new',
            'image',
            'available',
            'prompt',
        ];
        foreach ($fields as $field) {
            $obj->{$field} = Arr::get($data,$field);
        }
        return $obj;
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'is_new' => (int) $this->is_new,
            'image' => $this->image,
            'available' => $this->available,
            'prompt' => $this->prompt,
            'sort' => $this->sort,
        ];
    }
}
