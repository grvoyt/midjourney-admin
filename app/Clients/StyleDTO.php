<?php

namespace App\Clients;

use Illuminate\Support\Arr;

class StyleDTO
{
    public ?string $uuid;
    public string $title;
    public bool $isNew;
    public ?string $image;
    public string $available;
    public string $prompt;
    public int $sort = 0;

    public static function from(array $values): self
    {
        $dto = new self;

        foreach($values as $key => $value) {
            if( $key == 'imgUrl' ) {
                $key = 'image';
            }

            if (property_exists($dto, $key)) {
                $dto->$key = $value;
            }
        }
//        dd([
//            $values,$dto
//        ]);
        return $dto;
    }

}
