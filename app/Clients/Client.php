<?php

namespace App\Clients;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class Client
{
    private string $token;
    private string $url;
    public function __construct()
    {
        $this->token = 'J4abc9NDQ67ZW9U3KcDV4rXr90dLKWdKOR2uDjyc';
        //$this->url = 'http://127.0.0.1:3002';
        $this->url = 'https://api.loorapp.com';
    }

    public function list(): Collection
    {
        $response = Http::withToken($this->token)
            ->get("{$this->url}/styles/list");

        $list = $response->json();

        return collect($list)->map(fn($item) => StyleDTO::from($item));
    }

    public function create(CreateRequestDTO $createRequestDTO): StyleDTO
    {
        $response = Http::withToken($this->token)
            ->post("{$this->url}/styles",$createRequestDTO->toArray());

        $style = StyleDTO::from($response->json());

        return $style;
    }

    public function show(string $uuid): StyleDTO
    {
        $response = Http::withToken($this->token)
            ->get("{$this->url}/styles/{$uuid}");

        $style = StyleDTO::from($response->json());

        return $style;
    }

    public function edit(string $uuid,CreateRequestDTO $createRequestDTO): StyleDTO
    {
        $response = Http::withToken($this->token)
            ->patch("{$this->url}/styles/{$uuid}",$createRequestDTO->toArray());

        $style = StyleDTO::from($response->json());

        return $style;
    }

    public function delete(string $uuid): bool
    {
        $response = Http::withToken($this->token)
            ->delete("{$this->url}/styles/{$uuid}");

        return $response->successful();
    }

    public function getMetrics(): Collection
    {
        $response = Http::withToken($this->token)
            ->get("{$this->url}/metrics/all");

        $metrics = $response->json();

        return collect($metrics);
    }

    public function getCharts(array $types): Collection
    {
        $response = Http::withToken($this->token)
            ->get("{$this->url}/metrics/charts",[
                'types' => Arr::join($types,',')
            ]);

        $charts = $response->json();

        return collect($charts);
    }
}
