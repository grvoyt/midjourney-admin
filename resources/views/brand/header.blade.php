@push('head')
    <link
        href="/favicon.ico"
        id="favicon"
        rel="icon"
    >
@endpush

<p class="h2 n-m font-thin v-center">
    <x-orchid-icon path="social-github"/>

    <span class="m-l d-none d-sm-block">
        Loor
        <small class="v-top opacity">admin</small>
    </span>
</p>
